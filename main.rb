
require 'benchmark'

puts (1..100).to_a.shuffle[0..2]
puts (1..100).to_a.sample(3)

Benchmark.bm do |x|
  x.report { (1..100).to_a.shuffle[0..2] }
  x.report { (1..100).to_a.sample(3) }
end


Just a little bit faster because it doesn't have to create a range.
