json.extract! meeting, :id, :name, :date_options, :location_options, :created_at, :updated_at
json.url meeting_url(meeting, format: :json)
