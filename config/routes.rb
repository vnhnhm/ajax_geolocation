Rails.application.routes.draw do
  resources :posts
  root 'posts#test'
  match 'retrieve-location/', to: 'posts#ajax_geolocation', via: 'post'
end
