class CreateMeetings < ActiveRecord::Migration[5.0]
  def change
    create_table :meetings do |t|
      t.string :name
      t.string :date_options, array: true, default: []
      t.string :location_options, array: true, default: []

      t.timestamps
    end
  end
end
